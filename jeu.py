"""
             Projet CyberAttack@IUT'O
        SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

    Module jeu.py
"""

import random
from case import get_protection, get_trojans_entrants
import plateau
import protection
import matrice


def creer_jeu(liste_joueurs, taille_plateau=5, resistance_serveur=40, resistance_pc=50,
              resistance_protection=2, humain=False, nb_tours_max=-1):
    """Créer un nouveau jeu avec 4 joueurs

    Args:
        liste_joueurs (list): la liste des noms de joueur
        taille_plateau (int, optional): le coté du plateau. Defaults to 5.
        resistance_serveur (int, optional): la resistance du serveur. Defaults to 4.
        resistance_pc (int, optional): la resistance des PC. Defaults to 5.
        resistance_protection (int, optional): la resistance des protections. Defaults to 2.
        humain (bool, optional): indique si le joueur 1 est humain. Defaults to False.
        nb_tours_max (int, optional): indique le nombre de tours de la partie
                                      (-1 pour indiquer pas de limite). Defaults -1.

    Returns:
        dict: le jeu
    """
    jeu = dict()
    jeu['les_plateaux'] = []
    for i in range(len(liste_joueurs)):
        jeu['les_plateaux'].append(plateau.creer_plateau(
            i+1, liste_joueurs[i], taille_plateau, resistance_serveur, resistance_pc, 0))
    jeu['humain'] = humain
    jeu["resistance_protection"] = resistance_protection
    jeu['nb_tours_max'] = nb_tours_max
    jeu['nb_tours'] = 0
    jeu["liste_joueurs_elimines"] = []

    return jeu


def get_taille_plateau(jeu):
    """Retourne la taille des plateau

    Args:
        jeu (dict): un jeu

    Returns:
        int: la taille des plateau
    """
    return plateau.get_taille(jeu['les_plateaux'][0])


def get_plateau(jeu, id_joueur):
    """ retourne le plateau de joueur indenfié par id_joueur

    Args:
        jeu (dict): un jeu
        id_joueur (int): l'identifiant du joueur (entre 1 et 4)

    Returns:
        dict: le plateau du joueur
    """
    for le_plateau in jeu['les_plateaux']:
        if plateau.get_id_joueur(le_plateau) == id_joueur:
            return le_plateau


def est_fini(jeu):
    """indique si la partie est terminée

    Args:
        jeu (dict): un jeu

    Returns:
        bool: un booléen à True si au moins trois joueur sont éliminés ou
              que le nombre de tours max est atteint
    """
    if jeu['nb_tours'] == jeu['nb_tours_max']:
        return True

    for le_plateau in jeu['les_plateaux']:
        if plateau.a_perdu(le_plateau) == True:
            jeu["liste_joueurs_elimines"].append((plateau.get_id_joueur(
                le_plateau), plateau.get_points(le_plateau), plateau.get_nom_joueur(le_plateau)))
    if len(jeu["liste_joueurs_elimines"]) >= 3:
        return True

    return False


def get_num_tour(jeu):
    """retourne le numéro du tour en cours

    Args:
        jeu (dict): un jeu

    Returns:
        int: le numéro du tour
    """
    return jeu['nb_tours']


def echange_trojans(jeu):
    """Effectue les échanges de trojans entre les joueurs (des sorties vers les entrées)

    Args:
        jeu (dict): un jeu
    """
    for le_plateau in jeu['les_plateaux']:
        plateau.set_trojans_entrants(le_plateau, [])

    for le_plateau in jeu['les_plateaux']:
        if plateau.get_trojans_sortants(le_plateau, 'B') != []:
            id_haut = plateau.id_joueur_haut(le_plateau)
            liste_trojans_B = plateau.get_trojans_sortants(le_plateau, 'B')
            plateau.ajouter_trojan(get_plateau(jeu, id_haut), liste_trojans_B)

        if plateau.get_trojans_sortants(le_plateau, 'H') != []:
            id_haut = plateau.id_joueur_haut(le_plateau)
            liste_trojans_H = plateau.get_trojans_sortants(le_plateau, 'H')
            plateau.ajouter_trojan(get_plateau(jeu, id_haut), liste_trojans_H)

        if plateau.get_trojans_sortants(le_plateau, 'D') != []:
            id_droite = plateau.id_joueur_droite(le_plateau)
            liste_trojans_D = plateau.get_trojans_sortants(le_plateau, 'D')
            plateau.ajouter_trojan(get_plateau(
                jeu, id_droite), liste_trojans_D)
        if plateau.get_trojans_sortants(le_plateau, 'G') != []:
            id_gauche = plateau.id_joueur_gauche(le_plateau)
            liste_trojans_G = plateau.get_trojans_sortants(le_plateau, 'G')
            plateau.ajouter_trojan(get_plateau(
                jeu, id_gauche), liste_trojans_G)
        plateau.reinit_les_sorties(le_plateau)


def diriger_trojan(jeu):
    """Applique la protection DONNEES_PERSONNELLES sur les quatre plateaux

    Args:
        jeu (dict): un jeu
    """
    for le_plateau in jeu["les_plateaux"]:
        plateau.diriger_trojan(le_plateau)


def phase1(jeu):
    """Effectue les déplacements des trojans sur les 4 plateaux

    Args:surf_serveur=self.images_serveurs_ajustees[niv_protection]
        jeu ((dict): un jeu
    """
    for le_plateau in jeu['les_plateaux']:
        plateau.deplacer_trojan_phase1(le_plateau)


def phase2(jeu):
    """Finalise les déplacements des trojans sur les 4 plateaux.
       cette fonction doit augementer le numero du tour de jeu de 1

    Args:
        jeu ((dict): un jeu
    """
    for le_plateau in jeu['les_plateaux']:
        plateau.deplacer_trojan_phase2(le_plateau)

    jeu["nb_tours"] += 1


# RECOPIER A PARTIR D'ICI DANS VOTRE FICHIER

def joueur_humain():
    """

    Returns:
        str: une chaine de caractères indiquant les ordres donnés par la personne
    """
    ("indiquez le direction de votre avatar")
    res = input()

    rep = input(
        "Souhaitez vous (P)oser une protection ou (A)ttaquer les adversaires? (P/A)")
    res += rep
    if rep == 'P':
        print(
            "indiquez le type de protection [O"+str(protection.PAS_DE_PROTECTION)+"]")
        type_protection = input()
        try:
            type_protection = int(type_protection)
        except:
            type_protection = protection.PAS_DE_PROTECTION
        if type_protection != protection.PAS_DE_PROTECTION:
            print("indiquez la position de votre protection")
            ligne = input("numero de la ligne ")
            colonne = input("numero de la colonne")
            try:
                ligne = int(ligne)
                colonne = int(colonne)
            except:
                type_protection = protection.PAS_DE_PROTECTION
        res += str(type_protection)+str(ligne)+str(colonne)
    elif rep == 'A':
        for direction in "GHD":
            print("indiquez le type de virus à envoyer vers "+direction)
            try:
                type_vir = int(input())
            except:
                type_vir = -1
            res += direction+str(type_vir)
    return res


def joueur_aleatoire(le_plateau):
    """produit des ordres aléatoires

    Args:
        le_plateau (dict): un plateau

    Returns:
        str: une chaine de caractères donnant des ordres compatibles mais aléatoires
        Les ordres sont donnés sous la forme
        d'une chaine de caractères dont les deux premiers indique le déplacement de l'avatar
        le troisième caractère est
        soit un A pour une attaque
        soit un P pour une protection
        En cas d'attaque, les caractères suivants sont GxHyDz où
                    x y et z sont des chiffres entre 0 et 4 indiquant le numéro de la
                             ligne ou de la colonne où sera envoyé le trojan
        En cas de pose d'une protection les caractère suivants seront trois chiffre tlc où
                    t est le type de la protection
                    l la ligne où poser la protection
                    c la colonne où poser la protection
    """
    # choix du déplacement de l'avatar
    res = random.choice(list(plateau.DIRECTIONS_AVATAR))
    taille = plateau.get_taille(le_plateau)
    # choix entre poser une protection ou attaquer les adversaires
    if random.randint(0, 1) == 0:
        ligne = random.randint(0, taille-1)
        colonne = random.randint(0, taille-1)
        ind_protect = random.randint(0, protection.PAS_DE_PROTECTION-1)
        if ligne != taille//2 or colonne != taille//2:
            res += 'P'+str(ind_protect)+str(ligne)+str(colonne)
    else:  # on attaque les adversaires
        res += 'A'
        les_voisins = ['G', 'H', 'D']
        for direct in les_voisins:
            res += direct+str(random.randint(0, 4))
    return res


def actions_joueur(jeu):
    """Récolte et exécute les actions choisies par chacun des joueurs

    Args:
        jeu (dict): un jeu
    """
    for id_joueur in range(1, 5):
        le_plateau = get_plateau(jeu, id_joueur)
        if plateau.a_perdu(le_plateau):
            continue
        if id_joueur == 1 and est_humain(jeu):
            ordres = joueur_humain()
        else:
            ordres = joueur_aleatoire(le_plateau)

        plateau.executer_ordres(le_plateau, ordres)
    echange_trojans(jeu)


def actions_joueur_ext(jeu, ordres):
    """Permet de faire jouer chaque joueur un tour de jeu

    Args:
        jeu (dict): le jeu sur lequel on joue
        ordres (dict): un dictionnaire dont les clés sont les numéros de joueur 
                       et les valeurs les str donnant les ordres de chaque joueur
    """
    for id_joueur in range(1, 5):
        le_plateau = get_plateau(jeu, id_joueur)
        if plateau.a_perdu(le_plateau):
            continue
        plateau.executer_ordres(le_plateau, ordres[id_joueur])
    echange_trojans(jeu)


def jeu_2_str(jeu, sep="\n||----||\n"):
    """Transforme un jeu en str pour le transfert via le réseau

    Args:
        jeu (dict): le jeu à transformer
        sep (str, optional): ce qui sépare deux plateaux. Defaults to "\n||----||\n".

    Returns:
        str: la chaine de caractères qui encode le jeu
    """
    type_joueur = 'O'
    if est_humain(jeu):
        type_joueur = 'H'
    res = str(get_num_tour(jeu))+';'+str(get_nb_tours_max(jeu))+';'+type_joueur
    for i in range(1, 5):
        res += sep+plateau.plateau_2_str(get_plateau(jeu, i))
    return res


def creer_jeu_from_str(jeu_str, sep="\n||----||\n"):
    """creer un jeu à partir d'une chaine de caractères

    Args:
        jeu_str (str): la chaine de caractères qui encode le jeu
        sep (str, optional): le séparateur de plateau. Defaults to "\n||----||\n".

    Returns:
        dict: le jeu codé dans la chaine de caractères
    """
    plateaux = jeu_str.split(sep)
    nb_tours, nb_tours_max, type_joueur = plateaux[0].split(";")
    nb_tours = int(nb_tours)
    nb_tours_max = int(nb_tours_max)
    humain = type_joueur == 'H'
    liste_plateaux = []
    for ind in range(1, len(plateaux)):
        liste_plateaux.append(plateau.creer_plateau_from_str(plateaux[ind]))
    return creer_jeu_en_cours(nb_tours, nb_tours_max, humain, liste_plateaux)


def sauver_jeu(jeu, nom_fic):
    """sauvegarde un jeu dans un fichier

    Args:
        jeu (dict): le jeu à sauvegarder
        nom_fic (str): le nom du fichier où sauvegarder le jeu
    """
    with open(nom_fic, "w") as fic:
        fic.write(jeu_2_str(jeu))


def charger_jeu(nom_fic):
    """créer un jeu à partir d'une sauvegarde

    Args:
        nom_fic (str): le nom du fichier

    Returns:
        dict: le jeu lu dans le fichier
    """
    with open(nom_fic) as fic:
        chaine = fic.read()
        return creer_jeu_from_str(chaine)


# fonctions additionnelles sur le jeu
def set_nom_joueur(jeu, id_joueur, nom_joueur):
    """change le nom du joueur numéro id_joueur

    Args:
        jeu (dict): le jeu
        id_joueur (int): un nombre entre 1 et 4 indiquant le joueur que l'on veut modifier
        nom_joueur (str): le nom du joueur
    """
    for le_plateau in jeu['les_plateaux']:
        if plateau.get_id_joueur(le_plateau) == id_joueur:
            plateau.set_nom_joueur(le_plateau, nom_joueur)


def est_humain(jeu):
    """Indique si le joueur 1 est humain ou non

    Args:
        jeu (dict): le jeu

    Returns:
        bool: True si le joueur 1 est humain
    """
    joueur1 = jeu['les_plateaux']
    if joueur1[jeu['humain']] == True:
        return True
    return False


def get_nb_tours_max(jeu):
    """Retourne le nombre de tours maximum pour la partie

    Args:
        jeu (dict): le jeu

    Returns:
        int: le nombre de tours maximum du jeu 
    """
    return jeu["nb_tours_max"]


def creer_jeu_en_cours(num_tours, nb_tours_max, humain, liste_plateaux):
    """crée un jeu à partir des informations donnés en paramètres. liste_plateaux
       donne la liste des plateaux dans l'ordre des joueurs (liste_plateaux[0] est le plateau du joueur 1 etc.)


    Args:
        num_tours (int): le numéro du tour
        nb_tours_max (int): le nombre de tours maximum
        humain (bool): True si le joueur 1 est un humain
        liste_plateaux (list): la liste des 4 plateaux des 4 joueurs
    """
    liste_joueur = []
    for le_plateau in liste_plateaux:
        liste_joueur.append(plateau.get_nom_joueur(le_plateau))
    jeu = creer_jeu(liste_joueur, taille_plateau=5, resistance_serveur=50,
                    resistance_pc=40, humain=humain, nb_tours_max=nb_tours_max)
    jeu['Les_plateaux'] = liste_plateaux
    jeu['nb_tours'] = num_tours


def strategie(le_plateau):
    print('ça joue')
    # choix deplacement avatar
    position_avatar = plateau.get_pos_avatar(le_plateau)
    la_meilleur_position = matrice.direction_max_voisin(
        plateau.get_matrice(le_plateau), position_avatar[0], position_avatar[1])
    taille = plateau.get_taille(le_plateau)

    le_choix = la_meilleur_position
    # choix pour protection
    if get_num_tour % 2 == 1:
        le_choix += 'p'
        le_choix += protection.ANTIVIRUS
        le_choix
    else:
        le_choix += 'A'
        les_voisins = ['G', 'H', 'D']
        for direct in les_voisins:
            le_choix += direct+str(4)+str(4)+str(4)

    return le_choix


def joueur_ia(jeu, id_joueur):
    """calcule les action du joueur id_joueur en fonction de l'état du jeu

    Args:
        jeu (dict): le jeu
        id_joueur (int): un nombre entre 1 et 4 indiquant quel joueur doit jouer
    Returns:
        str: la chaine de caractères donnant les ordres choisis par le joueur
    """
    # à titre d'exemple
    le_plateau = get_plateau(jeu, id_joueur)
    return joueur_aleatoire(le_plateau)
