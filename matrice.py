"""
             Projet CyberAttack@IUT'O
        SAÉ1.01 département Informatique IUT d'Orléans 2021-2022

    Module matrice.py
    Ce module de gestion des matrices
"""


def creer_matrice(nb_lig, nb_col, val_defaut=None):
    """créer une matrice contenant nb_lig lignes et nb_col colonnes avec
       pour valeur par défaut val_defaut

    Args:
        nb_lig (int): un entier strictement positif
        nb_col (int): un entier strictement positif
        val_defaut (Any, optional): La valeur par défaut des éléments de la matrice.
                                    Defaults to None.
    Returns:
        dict: la matrice
    """
    matrice = dict()
    matrice['nb_lignes'] = nb_lig
    matrice['nb_colonnes'] = nb_col
    matrice['valeurs'] = [val_defaut]*nb_lig*nb_col
    return matrice


def get_nb_lignes(matrice):
    """retourne le nombre de lignes de la matrice

    Args:
        matrice (dict): une matrice

    Returns:
        int: le nombre de lignes de la matrice
    """
    return matrice['nb_lignes']


def get_nb_colonnes(matrice):
    """retourne le nombre de colonnes de la matrice

    Args:
        matrice (dict): une matrice

    Returns:
        int: le nombre de colonnes de la matrice
    """
    return matrice['nb_colonnes']


def get_val(matrice, lig, col):
    """retourne la valeur en lig, col de la matrice

    Args:
        matrice (dict): une matrice
        lig (int): numéro de la ligne (en commençant par 0)
        col (int): numéro de la colonne (en commençant par 0)

    Returns:
        Any: la valeur en lig, col de la matrice
    """
    nb_colonne = get_nb_colonnes(matrice)
    return matrice['valeurs'][lig*nb_colonne+col]


def set_val(matrice, lig, col, val):
    """stocke la valeur val en lig, col de la matrice

    Args:
        matrice (dict): une matrice
        lig (int): numéro de la ligne (en commençant par 0)
        col (int): numéro de la colonne (en commençant par 0)
        val (Any): la valeur à stocker
    """
    nb_colonne = get_nb_colonnes(matrice)
    matrice['valeurs'][lig*nb_colonne+col] = val


def max_matrice(matrice, interdits=None):
    """retourne la liste des coordonnées des cases contenant la valeur la plus grande de la matrice
        Ces case ne doivent pas être parmi les interdits.

    Args:
        matrice (dict): une matrice
        interdits (set): un ensemble de tuples (ligne,colonne) de case interdites. Defaults to None

    Returns:
        list: la liste des coordonnées de cases de valeur maximale dans la matrice (hors cases interdites)
    """
    liste_coord = []
    valeur_max = sorted(list(set(matrice['valeurs'])), reverse=True)
    print(valeur_max)
    indice_nb_max = 0
    while liste_coord == []:
        for ligne in range(get_nb_lignes(matrice)):
            for colonne in range(get_nb_colonnes(matrice)):
                if get_val(matrice, ligne, colonne) == valeur_max[indice_nb_max]:
                    if interdits != None and (ligne, colonne) not in interdits:
                        liste_coord.append((ligne, colonne))
                    elif interdits is None:
                        liste_coord.append((ligne, colonne))
        indice_nb_max += 1
    return liste_coord


DICO_DIR = {(-1, 1): 'HD', (-1, 0): 'HH', (-1, -1): 'HG', (0, -1): 'GG',
            (0, 1): 'DD', (1, -1): 'BG', (1, 0): 'BB', (1, 1): 'BD'}


def direction_max_voisin(matrice, ligne, colonne):
    """retourne la liste des directions qui permettent d'aller vers la case voisine de 
       la case (ligne,colonne) la plus grande. Le résultat doit aussi contenir la 
       direction qui permet de se rapprocher du milieu de la matrice
       si ligne,colonne n'est pas le milieu de la matrice

    Args:
        matrice (dict): une matrice
        ligne (int): le numéro de la ligne de la case considérée
        colonne (int): le numéro de la colonne de la case considérée

    Returns:
        str: deux lettres indiquant la direction DD -> droite , HD -> Haut Droite,
                                                 HH -> Haut, HG -> Haut gauche,
                                                 GG -> Gauche, BG -> Bas Gauche, BB -> Bas
    """
    liste_direction_possible = []
    liste_de_valeurs = list()
    for coord_dir, dir in DICO_DIR.items():
        if -1 < (ligne+coord_dir[0]) < get_nb_lignes(matrice) and -1 < (colonne+coord_dir[1]) < get_nb_colonnes(matrice):
            valeur = get_val(
                matrice, (ligne+coord_dir[0]), (colonne+coord_dir[1]))
            liste_de_valeurs.append((valeur, dir))
            if (ligne+coord_dir[0]) == 2 and (colonne+coord_dir[1]) == 2:
                liste_direction_possible.append(dir)

    print(liste_de_valeurs)

    valeur_max = max(liste_de_valeurs)[0]

    print(valeur_max)
    for elem in liste_de_valeurs:
        if elem[0] == valeur_max:
            liste_direction_possible.append(elem[1])

    return liste_direction_possible
